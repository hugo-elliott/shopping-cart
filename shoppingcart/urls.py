from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from shop import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('shop/', include('shop.urls')),
    path('search/', include('search.urls')),
    path('cart/', include('cart.urls')),
    path('order/', include('order.urls')),
    path('accounts/create', views.signUpView, name='signup'),
    # path('accounts/signin', views.signInView, name='signin'),
    path('accounts/login', auth_views.LoginView.as_view(), name='login'),
    path('accounts/logout', auth_views.LogoutView.as_view(), name='logout'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# alternative taught in https://www.udemy.com/develop-a-shopping-cart-website-with-django-2-and-python-3
# if settings.DEBUG:
#  urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
