from django.contrib import admin
from .models import Order, OrderItem


class OrderItemAdmin(admin.TabularInline):
    model = OrderItem
    fieldsets = [
        ('Product', {'fields': ['product'], }),
        ('Quantity', {'fields': ['quantity'], }),
        ('Price', {'fields': ['price'], }), ]
    readonly_fields = ['product', 'quantity', 'price']
    # remove the option to delete items from the order
    can_delete = False
    # removes the option to add an item to the order
    max_num = 0
    template = 'admin/order/tabular.html'


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'billingName', 'email', 'created']
    list_display_links = ['id', 'billingName']
    search_fields = ['id', 'billingName', 'email']
    readonly_fields = [
        'id',
        'token',
        'total',
        'email',
        'created',
        'billingName',
        'billingAddress1',
        'billingCity',
        'billingPostCode',
        'billingCountry',
        'shippingName',
        'shippingAddress1',
        'shippingCity',
        'shippingPostCode',
        'shippingCountry']
    fieldsets = [
        ('ORDER INFORMATION', {'fields': [
         'id', 'token', 'total', 'created'], }),
        ('BILLING INFORMATION', {'fields': [
            'billingName',
            'billingAddress1',
            'billingCity',
            'billingPostCode',
            'billingCountry',
            'email'], }),
        ('SHIPPING INFORMATION', {'fields': [
            'shippingName',
            'shippingAddress1',
            'shippingCity',
            'shippingPostCode',
            'shippingCountry'], }), ]
    inlines = [OrderItemAdmin, ]

    # prevents deletion of order records in the back end
    def has_delete_permission(self, request, obj=None):
        return False

    # prevents addition of order records in the back end
    def has_add_permission(self, request):
        return False
