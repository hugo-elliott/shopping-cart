from django.urls import path
from . import views

app_name='shop'

urlpatterns=[
  # maps all products
  path('',views.allProdCat, name='allProdCat'),
  # maps all products by category
  path('<slug:c_slug>/', views.allProdCat, name='products_by_category'),
  # map the product detail page
  path('<slug:c_slug>/<slug:product_slug>/',
    views.ProdCatDetail, name='ProdCatDetail'),
]
